<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Localization
Route::get('/js/lang-js', function () {
    if(env('APP_ENV','none') === 'local'){
        Cache::forget('lang-js');
    }
    $strings = Cache::rememberForever('lang-js', function () {
        $lang = config('app.locale');

        $files   = glob(resource_path('lang/' . $lang . '/*.php'));
        $strings = [];

        foreach ($files as $file) {
            $name           = basename($file, '.php');
            $strings[$name] = require $file;
        }

        return $strings;
    });

    header('Content-Type: text/javascript');
    echo('window.i18n = ' . json_encode($strings) . '');
    exit();
})->name('assets.lang');

Route::get('/{any}', function () {
    return view('app');
})->where('any', '.*');

// Auth::routes([
//     'reset' => false,
//     'verify' => false,
//     'register' => false,
// ]);

// Route::get('/home', 'HomeController@index')->name('home');
