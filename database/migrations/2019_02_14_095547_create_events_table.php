<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('moderator_id');
            $table->foreign('moderator_id')->references('id')->on('moderators')->onDelete('cascade');;
            $table->string('name');
            $table->string('cluster');
            $table->string('subcluster');
            $table->string('type');
            $table->string('partner');
            $table->string('language');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->integer('r');
            $table->integer('g');
            $table->integer('b');
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
