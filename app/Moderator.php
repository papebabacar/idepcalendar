<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moderator extends Model
{
    protected $fillable = ['email','firstname','lastname'];

    public function events()
    {
        return $this->hasMany('App\Event');
    }
}
