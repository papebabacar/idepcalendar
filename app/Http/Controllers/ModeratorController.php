<?php

// ModeratorController.php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ModeratorCollection;
use App\Moderator;

class ModeratorController extends Controller
{
    public function store(Request $request)
    {
        $moderator = new Moderator([
            'email' => $request->get('email'),
            'firstname' => $request->get('firstname'),
            'lastname' => $request->get('lastname')
        ]);

        $moderator->save();

        return response()->json([
            'success' => true,
            'moderators' => new ModeratorCollection(Moderator::all())
        ]);
    }

    public function index()
    {
        return new ModeratorCollection(Moderator::all());
    }

    public function edit($id)
    {
        $moderator = Moderator::find($id);
        return response()->json($moderator);
    }

    public function update($id, Request $request)
    {
        $moderator = Moderator::find($id);

        $moderator->update($request->all());

        return response()->json([
            'success' => true,
            'moderators' => new ModeratorCollection(Moderator::all())
        ]);
    }

    public function delete($id)
    {
        $moderator = Moderator::find($id);

        $moderator->delete();

        return response()->json([
            'success' => true,
            'moderators' => new ModeratorCollection(Moderator::all())
        ]);
    }
}