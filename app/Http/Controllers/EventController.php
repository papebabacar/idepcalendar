<?php

// EventController.php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\EventCollection;
use App\Event;

class EventController extends Controller
{
    public function store(Request $request)
    {
        $event = new Event([
            'name' => $request->get('name'),
            'moderator_id' => $request->get('moderator_id'),
            'type' => $request->get('type'),
            'language' => $request->get('language'),
            'partner' => $request->get('partner'),
            'cluster' => $request->get('cluster'),
            'subcluster' => $request->get('subcluster'),
            'start' => $request->get('start'),
            'end' => $request->get('end'),
            'r' => $request->get('r'),
            'g' => $request->get('g'),
            'b' => $request->get('b'),
            'active' => $request->get('active')
        ]);

        $event->save();

        return response()->json([
            'success' => true,
            'events' => new EventCollection(Event::all())
        ]);
    }

    public function index()
    {
        return new EventCollection(Event::all());
    }

    public function edit($id)
    {
        $event = Event::find($id);
        return response()->json($event);
    }

    public function update($id, Request $request)
    {
        $event = Event::find($id);

        $event->update($request->all());

        return response()->json([
            'success' => true,
            'events' => new EventCollection(Event::all())
        ]);
    }

    public function delete($id)
    {
        $event = Event::find($id);

        $event->delete();

        return response()->json([
            'success' => true,
            'events' => new EventCollection(Event::all())
        ]);
    }
}