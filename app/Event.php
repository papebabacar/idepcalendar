<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['name', 'moderator_id','type', 'language', 'partner', 'cluster', 'subcluster', 'start','end','r','g','b','active'];

    public function moderator()
    {
        return $this->belongsTo('App\Moderator');
    }

}
