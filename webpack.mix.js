const mix = require('laravel-mix')

mix
  .styles(
    [
      'resources/css/assembly.css',
      'resources/css/reset.css',
      'resources/css/colorpikr.css',
      'resources/css/autocomplete.css'
    ],
    'public/css/app.css'
  )
  .react('resources/js/app.js', 'public/js')
  .version()
  .sourceMaps()
  .browserSync('managetime.test')
