import React, { useState, useEffect } from 'react'
import {
  Box,
  FormGroup,
  Label,
  Input,
  Button,
  ControlFeedback,
  Modal,
  ModalDialog,
  ModalContent,
  ModalCloseButton,
  ModalHeader,
  Typography,
  ModalBody,
  ModalFooter,
  Toggler,
  css
} from '@smooth-ui/core-sc'
import { toast } from 'react-toastify'
import { Table, Row, HeaderRow, Header, Cell } from '../components/Table'
import { Edit } from 'styled-icons/typicons'
import { Delete } from 'styled-icons/material'
import { Plus } from 'styled-icons/icomoon'

import { get, post, delete as remove } from 'axios'

export default ({
  moderators,
  setModerators,
  history,
  match: {
    params: { id }
  }
}) => {
  const [email, setEmail] = useState('')
  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [edit, setEdit] = useState()
  const [submitted, setSubmitted] = useState()
  const [error, setError] = useState()

  useEffect(() => {
    if (id) {
      get(`/api/moderator/edit/${id}`).then(
        ({ data: { email, firstname, lastname } }) => {
          setEmail(email)
          setFirstname(firstname)
          setLastname(lastname)
          setEdit(true)
        }
      )
    }
  }, [id])

  const submit = e => {
    setError(false)
    e.preventDefault()
    setSubmitted(true)

    const uri = id ? `/api/moderator/update/${id}` : '/api/moderator/create'
    post(uri, {
      email,
      firstname,
      lastname: lastname.toUpperCase()
    })
      .then(({ data: { success, moderators = [] } }) => {
        if (!success) {
          setError(true)
          return
        }
        setModerators(moderators)
        setEdit(false)
        setSubmitted(false)
        history.push('/moderator')
        toast(
          id
            ? 'Modérateur modifié avec succès'
            : 'Modérateur ajouté avec succès',
          {
            position: toast.POSITION.BOTTOM_RIGHT,
            type: toast.TYPE.INFO
          }
        )
      })
      .catch(() => setError(true))
  }

  const onDelete = id => () => {
    try {
      remove(`/api/moderator/delete/${id}`).then(
        ({ data: { success, moderators = [] } }) => {
          if (!success) {
            setError(true)
            return
          }
          setModerators(moderators)
          history.push('/moderator')
          toast('Modérateur supprimé avec succès', {
            position: toast.POSITION.BOTTOM_RIGHT,
            type: toast.TYPE.INFO
          })
        }
      )
    } catch (e) {
      setError(true)
    }
  }

  return !edit ? (
    <Box
      css={css`
        width: 100%;
        height: 90%;
        background: #eee;
      `}
    >
      <Table>
        <tbody
          css={css`
            background: #fff;
          `}
        >
          <HeaderRow>
            <Header
              css={css`
                width: 41%;
              `}
            >
              Prénom
            </Header>
            <Header
              css={css`
                width: 23%;
              `}
            >
              Nom
            </Header>
            <Header
              css={css`
                width: 33%;
              `}
            >
              Email
            </Header>
            <Header>
              <a
                onClick={() => {
                  setEmail('')
                  setFirstname('')
                  setLastname('')
                  setEdit(true)
                }}
              >
                <Plus
                  css={css`
                    color: #4cac40;
                  `}
                  size={18}
                />
              </a>
            </Header>
            <Header />
          </HeaderRow>
          {moderators.map(({ id, email, firstname, lastname }) => (
            <Row
              key={id}
              onDoubleClick={() => history.push(`/moderator/${id}`)}
            >
              <Cell>{firstname}</Cell>
              <Cell>{lastname}</Cell>
              <Cell>{email}</Cell>
              <Header>
                <a onClick={() => history.push(`/moderator/${id}`)}>
                  <Edit
                    size="20"
                    css={css`
                      &:hover {
                        color: #40ac40;
                      }
                    `}
                  />
                </a>
              </Header>
              <Header>
                <Toggler>
                  {({ toggled, onToggle }) => (
                    <>
                      <a onClick={() => onToggle(true)}>
                        <Delete
                          size="20"
                          css={css`
                            &:hover {
                              color: #ec4040;
                            }
                          `}
                        />
                      </a>
                      <Modal opened={toggled} onClose={() => onToggle(false)}>
                        <ModalDialog maxWidth="28em" mx="auto">
                          <ModalContent>
                            <ModalCloseButton />
                            <ModalHeader>
                              <Typography variant="h5" m={0}>
                                Suppression
                              </Typography>
                            </ModalHeader>
                            <ModalBody
                              css={css`
                                color: #ec4040;
                              `}
                            >
                              Attention! voulez-vous supprimer ce modérateur?
                            </ModalBody>
                            <ModalFooter>
                              <Button
                                variant="secondary"
                                onClick={() => onToggle(false)}
                              >
                                Annuler
                              </Button>
                              <Button
                                variant="bgPrimary"
                                onClick={onDelete(id)}
                              >
                                Supprimer
                              </Button>
                            </ModalFooter>
                          </ModalContent>
                        </ModalDialog>
                      </Modal>
                    </>
                  )}
                </Toggler>
              </Header>
            </Row>
          ))}
        </tbody>
      </Table>
    </Box>
  ) : (
    <Box
      css={css`
        width: 100%;
        height: 90%;
        overflow-y: auto;
        display: flex;
        justify-content: center;
        align-items: center;
      `}
    >
      <form
        css={css`
          background: #fff;
          width: 100%;
          max-width: 28em;
          padding: 1.625em;
        `}
        onSubmit={submit}
      >
        <FormGroup>
          <Label>Prénom</Label>
          <Input
            control
            required
            autoFocus
            value={firstname}
            valid={submitted ? firstname !== '' : null}
            onChange={e => setFirstname(e.currentTarget.value)}
          />
        </FormGroup>

        <FormGroup>
          <Label>Nom</Label>
          <Input
            control
            required
            value={lastname}
            valid={submitted ? lastname !== '' : null}
            onChange={e => setLastname(e.currentTarget.value)}
          />
        </FormGroup>

        <FormGroup>
          <Label>Email</Label>
          <Input
            control
            required
            value={email}
            valid={submitted ? email !== '' : null}
            onChange={e => setEmail(e.currentTarget.value)}
            type="email"
          />
        </FormGroup>
        {error && (
          <ControlFeedback valid={false}>
            Une erreur est survenue
          </ControlFeedback>
        )}

        <Button css="float:right;" variant="bgPrimary">
          Enregistrer
        </Button>
        <Button
          css={css`
            float: right;
            margin-right: 0.8125em;
          `}
          variant="secondary"
          onClick={() => {
            setEdit(false)
            history.push('/moderator')
          }}
          type="cancel"
        >
          Annuler
        </Button>
      </form>
    </Box>
  )
}
