import React, { useState, useEffect } from 'react'
import {
  Box,
  Select,
  FormGroup,
  Label,
  Input,
  Button,
  ControlFeedback,
  FormCheck,
  FormCheckLabel,
  Checkbox,
  css
} from '@smooth-ui/core-sc'
import { toast } from 'react-toastify'
import DayPicker, { DateUtils } from 'react-day-picker'
import 'react-day-picker/lib/style.css'
import { get, post } from 'axios'

import MomentLocaleUtils from 'react-day-picker/moment'

export default ({
  periods,
  setPeriods,
  setEvents,
  currentPeriod,
  setCurrentPeriod
}) => {
  const [id, setId] = useState()
  const [name, setName] = useState('')
  const [edit, setEdit] = useState()
  const [add, setAdd] = useState()
  const [error, setError] = useState()
  const [range, setRange] = useState({ from: null, to: null })
  const [copy, setCopy] = useState(true)

  useEffect(() => {
    setEdit(periods.length === 0)
  }, [periods])

  useEffect(() => {
    const { id, name = '', start, end } =
      periods.find(({ id }) => id === currentPeriod) || {}
    setId(id)
    setName(name)
    start &&
      end &&
      setRange({
        from: new Date(start),
        to: new Date(end)
      })
  }, [currentPeriod, edit])

  useEffect(() => {
    add && setId(null)
    add && setName('')
    add && setRange({ from: null, to: null })
  }, [add])

  const submit = e => {
    setError(false)
    e.preventDefault()

    const uri = id ? `/api/period/update/${id}` : '/api/period/create'
    post(uri, {
      name,
      start: range.from,
      end: range.to
    })
      .then(({ data: { success, periods = [] } }) => {
        if (!success) {
          setError(true)
          return
        }
        setPeriods(periods)
        setEdit(false)
        setAdd(false)
        if (periods.length > 1) {
          get(`/api/period/${periods[periods.length - 2].id}/events`).then(
            ({ data }) => {
              data.map(event =>
                post('/api/event/create', {
                  ...event,
                  period_id: periods[periods.length - 1].id
                }).then(({ data: { success, events = [] } }) => {
                  if (!success) {
                    setError(true)
                    return
                  }
                  setEvents(events)
                })
              )
              toast(
                id
                  ? 'Période modifiée avec succès'
                  : 'Période ajoutée avec succès',
                {
                  position: toast.POSITION.BOTTOM_RIGHT,
                  type: toast.TYPE.INFO
                }
              )
            }
          )
        }
      })
      .catch(() => setError(true))
  }

  const onDayClick = day => {
    const newRange = DateUtils.addDayToRange(day, range)
    setRange(newRange)
  }

  return (
    <Box
      css={css`
        width: 100%;
        height: 95%;
        overflow-y: auto;
        background: #eee;
        display: flex;
        flex-direction: column;
        justify-content: center;
        .multi-date {
          border: 1px solid #ececec;
          margin: 0.40625em 0 0.40625em 0.40625em;
          background: #fff;
          min-height: 22em;
        }
        .multi-date
          .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
          background-color: #f0f8ff !important;
          color: #4a90e2;
        }
        .multi-date .DayPicker-Day {
          border-radius: 0 !important;
        }
        .multi-date .DayPicker-Day--start {
          border-top-left-radius: 50% !important;
          border-bottom-left-radius: 50% !important;
        }
        .multi-date .DayPicker-Day--end {
          border-top-right-radius: 50% !important;
          border-bottom-right-radius: 50% !important;
        }
      `}
    >
      <Box
        as="form"
        css={css`
          max-width: 38em;
          margin: 0 auto;
        `}
        onSubmit={submit}
      >
        <FormGroup>
          <Label css="color:#333;">Période</Label>
          {!edit ? (
            <Select
              control
              value={currentPeriod}
              onChange={e => setCurrentPeriod(Number(e.currentTarget.value))}
            >
              {periods.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </Select>
          ) : (
            <Input
              control
              required
              autoFocus
              value={name}
              onChange={e => setName(e.currentTarget.value)}
            />
          )}
        </FormGroup>
        <DayPicker
          className="multi-date"
          numberOfMonths={2}
          selectedDays={[range.from, range]}
          modifiers={{ start: range.from, end: range.to }}
          onDayClick={onDayClick}
          localeUtils={MomentLocaleUtils}
          locale="fr"
        />
        {!edit && (
          <a
            css={css`
              float: right;
              text-decoration: underline;
              color: #4040ec;
            `}
            onClick={() => setEdit(true)}
          >
            Modifier
          </a>
        )}
        {!edit && (
          <a
            css={css`
              float: right;
              text-decoration: underline;
              color: #4040ec;
              margin-right: 1.625em;
            `}
            onClick={() => {
              setEdit(true)
              setAdd(true)
            }}
          >
            Nouvelle période
          </a>
        )}
        {add && periods.length > 0 && (
          <FormCheck>
            <Checkbox
              variant="bgPrimary"
              id="active"
              checked={copy}
              onChange={() => setCopy(!copy)}
            />
            <FormCheckLabel htmlFor="active">Importer les cours</FormCheckLabel>
          </FormCheck>
        )}
        {edit && (
          <Button css="float:right;" variant="bgPrimary">
            Enregistrer
          </Button>
        )}
        {edit && periods.length > 0 && (
          <Button
            css={css`
              float: right;
              margin-right: 0.8125em;
            `}
            variant="secondary"
            onClick={() => {
              setEdit(false)
              setAdd(false)
            }}
            type="cancel"
          >
            Annuler
          </Button>
        )}
        {edit && error && (
          <ControlFeedback valid={false}>
            Veuillez choisir une date valide svp.
          </ControlFeedback>
        )}
      </Box>
    </Box>
  )
}
