import React, { useState } from 'react'
import { Box, css } from '@smooth-ui/core-sc'
import Search from '../components/Search'
import { Table, Row, HeaderRow, Header, Cell } from '../components/Table'
import moment from 'moment'

export default ({ events, moderators, history }) => {
  const [searchTerm, setSearchTerm] = useState('')
  return (
    <Box
      css={css`
        width: 100%;
        height: 90%;
        overflow-y: auto;
        background: #eee;
        display: flex;
        flex-direction: column;
        justify-content: center;
      `}
    >
      <Search
        events={events}
        searchTerm={searchTerm}
        setSearchTerm={setSearchTerm}
      />

      <Box
        css={css`
          width: 100%;
          height: 100%;
          background: #fff;
          padding: 0.8125em 1.625em;
          margin: 0 auto;
        `}
      >
        <Table>
          <tbody
            css={css`
              background: #fff;
            `}
          >
            <HeaderRow>
              <Header
                css={css`
                  width: 24%;
                `}
              >
                Cours
              </Header>
              <Header
                css={css`
                  width: 18%;
                `}
              >
                Cluster
              </Header>
              <Header
                css={css`
                  width: 15%;
                `}
              >
                Subcluster
              </Header>
              <Header
                css={css`
                  width: 10%;
                `}
              >
                Type
              </Header>
              <Header>Langue</Header>
              <Header
                css={css`
                  width: 16%;
                `}
              >
                Début
              </Header>
              <Header
                css={css`
                  width: 16%;
                `}
              >
                Fin
              </Header>
              <Header>Responsable</Header>
            </HeaderRow>
            {events.map(
              ({
                id,
                active,
                name,
                type,
                language,
                partner,
                cluster,
                subcluster,
                start,
                end,
                moderator_id: moderatorId
              }) => {
                if (
                  searchTerm &&
                  !normalize(name).includes(normalize(searchTerm)) &&
                  !normalize(cluster).includes(normalize(searchTerm)) &&
                  !normalize(subcluster).includes(normalize(searchTerm)) &&
                  !normalize(type).includes(normalize(searchTerm)) &&
                  !normalize(partner).includes(normalize(searchTerm)) &&
                  !normalize(language).includes(normalize(searchTerm))
                )
                  return null
                const { firstname, lastname } =
                  moderators.find(({ id }) => id === moderatorId) || {}
                return (
                  <Row key={id}>
                    <Cell>{name}</Cell>
                    <Cell>{cluster}</Cell>
                    <Cell>{subcluster}</Cell>
                    <Cell>{type}</Cell>
                    <Cell>{language}</Cell>
                    <Cell>
                      {moment(start)
                        .format('dddd DD MMMM YYYY')
                        .replace(/^\w/, c => c.toUpperCase())}
                    </Cell>
                    <Cell>
                      {moment(end)
                        .format('dddd DD MMMM YYYY')
                        .replace(/^\w/, c => c.toUpperCase())}
                    </Cell>
                    <Cell>
                      {firstname} {lastname}
                    </Cell>
                  </Row>
                )
              }
            )}
          </tbody>
        </Table>
      </Box>
    </Box>
  )
}

const normalize = string =>
  string
    .toLowerCase()
    .trim()
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
