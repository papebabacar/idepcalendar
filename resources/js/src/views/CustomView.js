import React, { useState } from 'react'
import moment from 'moment-business-days'
import {
  Box,
  Button,
  Modal,
  ModalDialog,
  ModalContent,
  ModalCloseButton,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Typography,
  css
} from '@smooth-ui/core-sc'
import { ArrowLeftThick, ArrowRightThick } from 'styled-icons/typicons'

const CustomView = ({ date, events, onDoubleClickEvent: edit, onNavigate }) => {
  const [selected, setSelected] = useState()
  const start = moment(date).startOf('month')
  const end = moment(date).endOf('month')
  let tmp = start.clone().startOf('isoWeek')
  let dates = {}
  while (tmp.isBefore(end.clone().endOf('isoWeek'))) {
    const startOfWeek = tmp
      .clone()
      .startOf('isoWeek')
      .toISOString()
    if (!dates[startOfWeek]) dates[startOfWeek] = []
    tmp.isBusinessDay() && dates[startOfWeek].push(tmp.clone())
    tmp.add(1, 'day')
  }
  const monthEvents = events.filter(
    ({ start: eventStart, end: eventEnd }) =>
      moment(eventStart).isBetween(start, end, 'day', '[]') ||
      moment(eventEnd).isBetween(start, end, 'day', '[]')
  )
  return (
    <>
      <Modal opened={selected !== undefined} onClose={() => setSelected()}>
        <ModalDialog maxWidth="32em" mx="auto">
          <ModalContent>
            <ModalCloseButton />
            {selected ? (
              <>
                <ModalHeader>
                  <Typography variant="h5" m={0}>
                    {selected.name}
                  </Typography>
                </ModalHeader>
                <ModalBody css={css``}>
                  <div>
                    <b>Cluster:</b> {selected.cluster}
                  </div>
                  <div>
                    <b>Subcluster:</b> {selected.subcluster}
                  </div>
                  <br />
                  <div>
                    <b>Type:</b> {selected.type}
                  </div>
                  <div>
                    <b>langue:</b> {selected.language}
                  </div>
                  <div>
                    <b>Partner/Sponsor:</b> {selected.partner}
                  </div>
                  <br />
                  <div>
                    {moment(selected.start)
                      .format('dddd DD MMMM YYYY')
                      .replace(/^\w/, c => c.toUpperCase())}
                  </div>
                  <div>
                    {moment(selected.end)
                      .format('dddd DD MMMM YYYY')
                      .replace(/^\w/, c => c.toUpperCase())}
                  </div>
                </ModalBody>
                <ModalFooter>
                  <Button variant="bgPrimary" onClick={() => edit(selected.id)}>
                    Modifier
                  </Button>
                </ModalFooter>
              </>
            ) : (
              ''
            )}
          </ModalContent>
        </ModalDialog>
      </Modal>
      <Box className="rbc-month-view">
        <Box className="rbc-month-header">
          <Box className="rbc-header">Lundi</Box>
          <Box className="rbc-header">Mardi</Box>
          <Box className="rbc-header">Mercredi</Box>
          <Box className="rbc-header">Jeudi</Box>
          <Box className="rbc-header">Vendredi</Box>
        </Box>
        {Object.entries(dates).map(([week, weekDates]) => (
          <Box key={week} className="rbc-month-row">
            <Box className="rbc-row-content">
              <Box className="rbc-row">
                {weekDates.map(date => (
                  <Box
                    key={date.toISOString()}
                    className={`rbc-date-cell rbc-day-bg ${
                      date.isBefore(start) ? 'rbc-off-range-bg' : ''
                    } ${date.isAfter(end) ? 'rbc-off-range-bg' : ''} ${
                      date.isSame(moment(), 'day') ? 'rbc-today' : ''
                    }`}
                  >
                    <span>{date.format('DD')}</span>
                    {events.map(event => {
                      const {
                        id,
                        name,
                        subcluster,
                        start: eventStart,
                        end: eventEnd,
                        r,
                        g,
                        b
                      } = event
                      if (
                        date.isBetween(
                          moment(eventStart),
                          moment(eventEnd),
                          'day',
                          '[]'
                        )
                      )
                        return (
                          <Box
                            className="event"
                            data-tip
                            onClick={() => setSelected(event)}
                            style={{
                              '--red': r,
                              '--green': g,
                              '--blue': b
                            }}
                            key={id}
                          >
                            <span>• {name}</span>
                            <span>{subcluster}</span>
                          </Box>
                        )
                    })}
                  </Box>
                ))}
              </Box>
            </Box>
          </Box>
        ))}
      </Box>
      <Box
        css={css`
          margin: 1em 0 0;
          padding: 0.203125em;
          border: 1px solid #ccc;
          Box:nth-of-type(odd) {
            background: #f1f2f4;
          }
        `}
      >
        <Box
          css={css`
            text-align: center;
            font-weight: 900;
            padding: 0.40625em;
          `}
        >
          <a
            css={css`
              padding: 0.4625em;
            `}
            onClick={() => onNavigate('PREV')}
          >
            <ArrowLeftThick size="24" />
          </a>
          Evénements {moment(date).format('MMMM YYYY')}
          <a
            css={css`
              padding: 0.4625em;
            `}
            onClick={() => onNavigate('NEXT')}
          >
            <ArrowRightThick size="24" />
          </a>
        </Box>
        {!monthEvents.length ? (
          <Box
            css={css`
              padding: 0.4625em;
              text-align: center;
            `}
          >
            Aucun évènement ce mois-ci
          </Box>
        ) : (
          monthEvents.map(event => (
            <Box
              key={event.id}
              css={css`
                padding: 0.4625em;
                &:nth-of-type(odd) {
                  background: #efefef;
                }
                span {
                  vertical-align: top;
                  display: inline-block;
                  width: 15%;
                }
                a {
                  vertical-align: top;
                  display: inline-block;
                  width: 30%;
                  color: #4040ec;
                }
                span::last-of-type {
                  width: 10%;
                }
              `}
            >
              <span>{`${moment(event.start)
                .format('dddd DD MMMM')
                .replace(/^\w/, c => c.toUpperCase())} - ${moment(event.end)
                .format('dddd DD MMMM')
                .replace(/^\w/, c => c.toUpperCase())}`}</span>
              <a onClick={() => setSelected(event)}>{event.name}</a>
              <span>{event.cluster}</span>
              <span>{event.subcluster}</span>
              <span>{event.type}</span>
            </Box>
          ))
        )}
      </Box>
    </>
  )
}

CustomView.range = date => {
  return {
    start: moment(date)
      .startOf('month')
      .toDate(),
    end: moment(date)
      .endOf('month')
      .toDate()
  }
}

CustomView.navigate = (date, action) => {
  switch (action) {
    case 'PREV':
      return moment(date)
        .add(-1, 'month')
        .toDate()

    case 'NEXT':
      return moment(date)
        .add(1, 'month')
        .toDate()

    default:
      return date
  }
}

CustomView.title = date =>
  `${moment(date)
    .format('MMMM')
    .replace(/^\w/, c => c.toUpperCase())} ${moment(date).format('YYYY')}`

export default CustomView
