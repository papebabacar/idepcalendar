import 'rc-time-picker/assets/index.css'
import React, { useState, useEffect, useMemo } from 'react'
import {
  Box,
  FormGroup,
  Label,
  Input,
  Select,
  Checkbox,
  FormCheck,
  FormCheckLabel,
  Button,
  ControlFeedback,
  Modal,
  ModalDialog,
  ModalContent,
  ModalCloseButton,
  ModalHeader,
  Typography,
  ModalBody,
  ModalFooter,
  Toggler,
  css
} from '@smooth-ui/core-sc'
import { Table, Row, HeaderRow, Header, Cell } from '../components/Table'
import { Edit } from 'styled-icons/typicons'
import { Delete } from 'styled-icons/material'
import { Plus } from 'styled-icons/icomoon'
import { BadgeCheck } from 'styled-icons/boxicons-solid'
import DayPicker, { DateUtils } from 'react-day-picker'
import 'react-day-picker/lib/style.css'
import ColorPicker from '@mapbox/react-colorpickr'
import { toast } from 'react-toastify'
import MomentLocaleUtils from 'react-day-picker/moment'
import moment from 'moment'
import { useTable, useSortBy } from 'react-table'

import { get, post, delete as remove } from 'axios'

const clusters = {
  'Macroeconomic development and planning': [
    'Data and statistics',
    'Economic Modelling',
    'Strategic planning and development policy',
    'Financing development'
  ],
  'Regional integration': ['Trade', 'Industry', 'Infrastructure'],
  'Social development': ['Gender and Youth', 'Human development', 'Migration'],
  'Integrated Natural resource management': [
    'Land, agriculture and food systems',
    'Naturals resources'
  ]
}
const colors = {
  'Macroeconomic development and planning': { r: 156, g: 194, b: 229 },
  'Regional integration': { r: 174, g: 170, b: 170 },
  'Social development': { r: 255, g: 217, b: 102 },
  'Integrated Natural resource management': { r: 168, g: 208, b: 141 }
}

const defaultCluster = Object.keys(clusters)[0]
const defaultSubcluster = clusters[defaultCluster][0]
const defaultColor = colors[defaultCluster]

export default ({
  events,
  setEvents,
  moderators,
  history,
  match: {
    params: { id }
  }
}) => {
  const [name, setName] = useState('')
  const [type, setType] = useState('Cours Présentiel')
  const [language, setLanguage] = useState('FR')
  const [partner, setPartner] = useState('IDEP')
  const [cluster, setCluster] = useState(defaultCluster)
  const [subcluster, setSubcluster] = useState(defaultSubcluster)
  const [range, setRange] = useState({ from: null, to: null })
  const [color, setColor] = useState({})
  const [active, setActive] = useState(true)
  const [moderatorId, setModeratorId] = useState()
  const [edit, setEdit] = useState()
  const [submitted, setSubmitted] = useState()
  const [error, setError] = useState()
  const [step, setStep] = useState(1)
  const { r, g, b } = color

  const data = useMemo(() => events, [events])

  const columns = useMemo(
    () => [
      {
        Header: 'Statut',
        accessor: 'active',
        Cell: ({ value: active }) => (
          <BadgeCheck
            css={css`
              color: ${active ? '#40ac40' : '#aaa'};
            `}
            size={24}
          />
        )
      },
      {
        Header: 'Cours',
        accessor: 'name',
        style: {
          width: '20%'
        }
      },
      {
        Header: 'Cluster',
        accessor: 'cluster',
        style: {
          width: '17%'
        }
      },
      {
        Header: 'Subcluster',
        accessor: 'subcluster',
        style: {
          width: '13%'
        }
      },
      {
        Header: 'Type',
        accessor: 'type',
        style: {
          width: '6%'
        }
      },
      {
        Header: 'Langue',
        accessor: 'language',
        style: {
          width: '5%'
        }
      },
      {
        Header: 'Début',
        accessor: 'start',
        Cell: ({ value: start }) =>
          moment(start)
            .format('dddd DD MMMM YYYY')
            .replace(/^\w/, c => c.toUpperCase()),
        style: {
          width: '16%'
        }
      },
      {
        Header: 'Fin',
        accessor: 'end',
        Cell: ({ value: end }) =>
          moment(end)
            .format('dddd DD MMMM YYYY')
            .replace(/^\w/, c => c.toUpperCase()),
        style: {
          width: '16%'
        }
      },
      {
        Header: 'Responsable',
        accessor: 'moderator_id',
        Cell: ({ value: moderatorId }) => {
          const { firstname, lastname } =
            moderators.find(({ id }) => id === moderatorId) || {}
          return `${firstname} ${lastname}`
        },
        style: {
          width: '12%'
        }
      },
      {
        Header: '',
        accessor: 'id',
        id: 'edit',
        Cell: ({ value: id }) => (
          <a onClick={() => history.push(`/event/${id}`)}>
            <Edit
              size="20"
              css={css`
                &:hover {
                  color: #40ac40;
                }
              `}
            />
          </a>
        )
      },
      {
        Header: '',
        accessor: 'id',
        id: 'delete',
        Cell: ({ value: id }) => (
          <Toggler>
            {({ toggled, onToggle }) => (
              <>
                <a onClick={() => onToggle(true)}>
                  <Delete
                    size="20"
                    css={css`
                      &:hover {
                        color: #ec4040;
                      }
                    `}
                  />
                </a>
                <Modal opened={toggled} onClose={() => onToggle(false)}>
                  <ModalDialog maxWidth="28em" mx="auto">
                    <ModalContent>
                      <ModalCloseButton />
                      <ModalHeader>
                        <Typography variant="h5" m={0}>
                          Suppression
                        </Typography>
                      </ModalHeader>
                      <ModalBody
                        css={css`
                          color: #ec4040;
                        `}
                      >
                        Attention! voulez-vous supprimer cet évènement?
                      </ModalBody>
                      <ModalFooter>
                        <Button
                          variant="secondary"
                          onClick={() => onToggle(false)}
                        >
                          Annuler
                        </Button>
                        <Button variant="bgPrimary" onClick={onDelete(id)}>
                          Supprimer
                        </Button>
                      </ModalFooter>
                    </ModalContent>
                  </ModalDialog>
                </Modal>
              </>
            )}
          </Toggler>
        )
      }
    ],
    [moderators]
  )

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow
  } = useTable(
    {
      data,
      columns
    },
    useSortBy
  )

  useEffect(() => {
    setModeratorId(moderatorId || (moderators.length && moderators[0].id))
  })

  useEffect(() => {
    if (id) {
      get(`/api/event/edit/${id}`).then(
        ({
          data: {
            active,
            name,
            type,
            language,
            partner,
            cluster,
            subcluster,
            start,
            end,
            r,
            g,
            b,
            moderator_id: moderatorId
          }
        }) => {
          setActive(active)
          setName(name)
          setType(type)
          setLanguage(language)
          setPartner(partner)
          setCluster(cluster)
          setSubcluster(subcluster)
          start &&
            end &&
            setRange({
              from: new Date(start),
              to: new Date(end)
            })
          setColor({ r, g, b })
          setModeratorId(moderatorId)
          setEdit(true)
          setStep(1)
        }
      )
    }
  }, [id])

  const onDayClick = day => {
    const newRange = DateUtils.addDayToRange(day, range)
    setRange(newRange)
  }

  const submit = e => {
    setError(false)
    e.preventDefault()
    if (!name) {
      setError('Le nom est obligatoire')
      setStep(1)
      return
    }
    if (!moment(range.from).isValid() || !moment(range.to).isValid()) {
      setError('Veuillez choisir deux dates')
      setStep(3)
      return
    }
    setSubmitted(true)
    const uri = id ? `/api/event/update/${id}` : '/api/event/create'
    post(uri, {
      name,
      type,
      language,
      partner,
      cluster,
      subcluster,
      start: range.from,
      end: range.to,
      active,
      r,
      g,
      b,
      moderator_id: moderatorId
    })
      .then(({ data: { success, events = [] } }) => {
        if (!success) {
          setError(
            'Une erreur est survenue, veuillez verifier les champs saisis'
          )
          return
        }
        setEvents(events)
        setEdit(false)
        setSubmitted(false)
        history.push('/event')
        toast(
          id ? 'Evénement modifié avec succès' : 'Evénement ajouté avec succès',
          {
            position: toast.POSITION.BOTTOM_RIGHT,
            type: toast.TYPE.INFO
          }
        )
      })
      .catch(() => setError('Une erreur est survenue.'))
  }

  const onDelete = id => () => {
    try {
      remove(`/api/event/delete/${id}`).then(
        ({ data: { success, events = [] } }) => {
          if (!success) {
            setError('Une erreur est survenue lors de la suppression.')
            return
          }
          setEvents(events)
          history.push('/event')
          toast('Evénement supprimé avec succès', {
            position: toast.POSITION.BOTTOM_RIGHT,
            type: toast.TYPE.INFO
          })
        }
      )
    } catch (e) {
      setError('Une erreur est survenue lors de la suppression.')
    }
  }

  if (moderators.length === 0) {
    return (
      <Box
        css={css`
          width: 100%;
          height: 90%;
          background: #eee;
          display: flex;
          justify-content: center;
          align-items: center;
        `}
      >
        <Button variant="bgPrimary" onClick={() => history.push('/moderator')}>
          Aucun responsable. Veuillez d'abord en ajouter un en cliquant ici.
        </Button>
      </Box>
    )
  }

  return !edit ? (
    <Box
      css={css`
        width: 100%;
        height: 90%;
        background: #eee;
      `}
    >
      <Table {...getTableProps()}>
        <thead>
          {headerGroups.map(headerGroup => (
            <HeaderRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                <Header
                  {...column.getHeaderProps([
                    {
                      style: column.style
                    },
                    column.getSortByToggleProps()
                  ])}
                >
                  {column.render('Header')}
                  <span>
                    {column.isSorted
                      ? column.isSortedDesc
                        ? ' 🔽'
                        : ' 🔼'
                      : ''}
                  </span>
                </Header>
              ))}
            </HeaderRow>
          ))}
        </thead>
        <tbody
          {...getTableBodyProps()}
          css={css`
            background: #fff;
          `}
        >
          {rows.map((row, i) => {
            prepareRow(row)
            return (
              <Row {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return (
                    <Cell {...cell.getCellProps()}>{cell.render('Cell')}</Cell>
                  )
                })}
              </Row>
            )
          })}
          <HeaderRow>
            <Header />
            <Header>
              <a
                onClick={() => {
                  setName('')
                  setType('Cours Présentiel')
                  setLanguage('FR')
                  setPartner('IDEP')
                  setCluster(defaultCluster)
                  setSubcluster(defaultSubcluster)
                  setRange({
                    from: null,
                    to: null
                  })
                  setColor(defaultColor)
                  setEdit(true)
                  setStep(1)
                }}
              >
                <Plus
                  css={css`
                    color: #4cac40;
                  `}
                  size={18}
                />{' '}
                Ajoutez un cours
              </a>
            </Header>
          </HeaderRow>
        </tbody>
      </Table>
    </Box>
  ) : (
    <Box
      css={css`
        width: 100%;
        height: 90%;
        overflow-y: auto;
        display: flex;
        justify-content: center;
        align-items: center;
        .multi-date {
          border: 1px solid #ececec;
          margin: 0.40625em 0 0.40625em 0.40625em;
          background: #fff;
          min-height: 22em;
        }
        .multi-date
          .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
          background-color: #f0f8ff !important;
          color: #4a90e2;
        }
        .multi-date .DayPicker-Day {
          border-radius: 0 !important;
        }
        .multi-date .DayPicker-Day--start {
          border-top-left-radius: 50% !important;
          border-bottom-left-radius: 50% !important;
        }
        .multi-date .DayPicker-Day--end {
          border-top-right-radius: 50% !important;
          border-bottom-right-radius: 50% !important;
        }
      `}
    >
      <form
        css={css`
          background: #fff;
          width: 100%;
          max-width: 610px;
          padding: 1.625em;
        `}
        onSubmit={submit}
      >
        {step === 1 && (
          <>
            <FormGroup>
              <Label>Cours</Label>
              <Input
                control
                required
                autoFocus
                value={name}
                valid={submitted ? name !== '' : null}
                onChange={e => setName(e.currentTarget.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label>Type</Label>
              <Select
                control
                value={type}
                onChange={e => setType(e.currentTarget.value)}
              >
                <option value="FR">Cours Présentiel</option>
                <option value="EN">Cours en ligne</option>
                <option value="Bilingue">Webinaire</option>
              </Select>
            </FormGroup>
            <FormGroup>
              <Label>Langue</Label>
              <Select
                control
                value={language}
                onChange={e => setLanguage(e.currentTarget.value)}
              >
                <option value="FR">FR</option>
                <option value="EN">EN</option>
                <option value="Bilingue">Bilingue</option>
              </Select>
            </FormGroup>
          </>
        )}
        {step === 2 && (
          <>
            <FormGroup>
              <Label>Cluster</Label>
              <Select
                control
                value={cluster}
                onChange={e => {
                  setCluster(e.currentTarget.value)
                  setColor(colors[e.currentTarget.value])
                }}
              >
                {Object.keys(clusters).map(cluster => (
                  <option key={cluster} value={cluster}>
                    {cluster}
                  </option>
                ))}
              </Select>
            </FormGroup>
            <FormGroup>
              <Label>Subcluster</Label>
              <Select
                control
                value={subcluster}
                onChange={e => setSubcluster(e.currentTarget.value)}
              >
                {clusters[cluster].map(subcluster => (
                  <option key={subcluster} value={subcluster}>
                    {subcluster}
                  </option>
                ))}
              </Select>
            </FormGroup>
            <FormGroup>
              <Label>Responsable</Label>
              <Select
                control
                value={moderatorId}
                onChange={e => setModeratorId(Number(e.currentTarget.value))}
              >
                {moderators.map(({ id, email, firstname, lastname }) => (
                  <option key={id} value={id}>
                    {firstname} {lastname} - {email}
                  </option>
                ))}
              </Select>
            </FormGroup>
            <FormCheck>
              <Checkbox
                id="active"
                checked={Boolean(active)}
                onChange={() => setActive(!active)}
              />
              <FormCheckLabel htmlFor="active">Actif</FormCheckLabel>
            </FormCheck>
            <FormGroup>
              <Label>Sponsor</Label>
              <Input
                control
                value={partner}
                onChange={e => setPartner(e.currentTarget.value)}
              />
            </FormGroup>

            <Box
              css={css`
                background: rgb(${r}, ${g}, ${b});
                margin-top: 0.8125em;
                margin-bottom: 0.8125em;
                height: 2.40625em;
              `}
            />
            <ColorPicker
              reset={false}
              initialValue={`rgb(${r},${g},${b})`}
              onChange={({ r, g, b }) => {
                setColor({ r, g, b })
              }}
            />
          </>
        )}
        {step === 3 && (
          <FormGroup>
            <DayPicker
              className="multi-date"
              numberOfMonths={2}
              selectedDays={[range.from, range]}
              modifiers={{ start: range.from, end: range.to }}
              onDayClick={onDayClick}
              localeUtils={MomentLocaleUtils}
              locale="fr"
            />
          </FormGroup>
        )}
        {error && <ControlFeedback valid={false}>{error}</ControlFeedback>}
        {step === 3 && (
          <Button css="float:right;" variant="bgPrimary">
            Enregistrer
          </Button>
        )}
        {step < 3 && (
          <Button
            css="float:right;"
            variant="bgPrimary"
            onClick={event => {
              event.preventDefault()
              setStep(step + 1)
            }}
          >
            Suivant
          </Button>
        )}
        {step > 1 && (
          <Button
            css={css`
              float: right;
              margin-right: 0.8125em;
            `}
            variant="bgPrimary"
            onClick={event => {
              event.preventDefault()
              setStep(step - 1)
            }}
          >
            Precedent
          </Button>
        )}
        <Button
          css={css`
            float: right;
            margin-right: 0.8125em;
          `}
          variant="secondary"
          onClick={() => {
            setEdit(false)
            history.push('/event')
          }}
          type="cancel"
        >
          Annuler
        </Button>
      </form>
    </Box>
  )
}
