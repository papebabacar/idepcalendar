import 'react-big-calendar/lib/css/react-big-calendar.css'
import { Box, css } from '@smooth-ui/core-sc'
import { ArrowLeftThick, ArrowRightThick } from 'styled-icons/typicons'
import CustomView from './CustomView'
import BigCalendar from 'react-big-calendar'
import React, { useState, useEffect } from 'react'
import moment from 'moment'

moment.locale('fr')
const localizer = BigCalendar.momentLocalizer(moment)

export default ({ events: allEvents, history }) => {
  const [events, setEvents] = useState([])

  useEffect(() => {
    setEvents(
      allEvents.reduce(
        (
          events,
          {
            id,
            name,
            type,
            language,
            partner,
            cluster,
            subcluster,
            start,
            end,
            r,
            g,
            b,
            active
          }
        ) => {
          if (!active) return events
          return [
            ...events,
            {
              id,
              name,
              type,
              language,
              partner,
              cluster,
              subcluster,
              start: moment(start).toDate(),
              end: moment(end).toDate(),
              r,
              g,
              b
            }
          ]
        },
        []
      )
    )
  }, [allEvents])

  return (
    <Box
      px={1}
      py={1}
      css={css`
        background: #fafafa;
        min-height: 95%;
        .rbc-calendar {
          font-size: 95%;
        }
        .rbc-toolbar {
          font-size: 105%;
        }
        .rbc-month-view {
          display: block;
          border: 1px solid #aaa;
        }
        .rbc-date-cell {
          min-height: 8em;
          margin: 1px;
          padding: 0;
          display: flex;
          flex-direction: column;
          > span {
            color: #4040ec;
          }
        }
        .event {
          cursor: pointer;
          flex: 1;
          margin-top: 0.203125em;
          padding: 0.6125em 0.203125em;
          text-align: left;
          background: rgb(var(--red), var(--green), var(--blue));
          --r: calc(var(--red) * 0.299);
          --g: calc(var(--green) * 0.587);
          --b: calc(var(--blue) * 0.114);
          --sum: calc(var(--r) + var(--g) + var(--b));
          --perceived-lightness: calc(var(--sum) / 255);
          color: hsl(
            0,
            0%,
            calc((var(--perceived-lightness) - var(--threshold)) * -10000000%)
          );
          --border-alpha: calc(
            (var(--perceived-lightness) - var(--border-threshold)) * 100
          );
          border-width: 0.2em;
          border-style: solid;
          border-color: rgba(
            calc(var(--red) - 50),
            calc(var(--green) - 50),
            calc(var(--blue) - 50),
            var(--border-alpha)
          );
          > span {
            display: block;
            line-height: 1.2em;
          }
          > span + span {
            margin-top: 0.5em;
          }
        }
      `}
    >
      <BigCalendar
        views={{
          custom: CustomView
        }}
        defaultView="custom"
        localizer={localizer}
        onDoubleClickEvent={id => history.push(`/event/${id}`)}
        events={events}
        startAccessor="start"
        endAccessor="end"
        length={6}
        scrollToTime={moment('2018-05-02 08:00').toDate()}
        popup
        messages={{
          previous: <ArrowLeftThick size="18" />,
          next: <ArrowRightThick size="18" />,
          month: 'Mois',
          day: 'Jour',
          week: 'Semaine',
          today: "aujourdh'ui",
          time: 'Heure',
          event: 'Evénement',
          allDay: 'Journée entière',
          noEventsInRange: 'Aucun événement prévu en cette période',
          showMore: count => `+${count} ${count > 1 ? 'autres' : 'autre'}`
        }}
      />
    </Box>
  )
}
