import { theme } from '@smooth-ui/core-sc'

export default {
  ...theme,
  primary: 'rgba(140,140,140,0.35)',
  bgPrimary: '#1e97ab',
  breakpoints: {
    xs: 0,
    sm: '576px',
    md: '768px',
    lg: '992px',
    xl: '1200px',
    xxl: '1920px'
  },
  gridMaxWidths: {
    sm: '540px',
    md: '720px',
    lg: '960px',
    xl: '1140px',
    xxl: '1860px'
  }
}
