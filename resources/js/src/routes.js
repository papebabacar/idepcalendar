import { Route, Switch } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import NavBar from './components/NavBar'
import App from './components/App'
import Calendar from './views/Calendar'
import Moderator from './views/Moderator'
import Event from './views/Event'
import Search from './views/Search'

import React, { useState, useEffect } from 'react'

import { get } from 'axios'

export const Routes = () => {
  const [moderators, setModerators] = useState([])
  const [events, setEvents] = useState([])

  useEffect(() => {
    get(`/api/moderators`).then(({ data: { data } }) => {
      setModerators(data)
    })
  }, [])

  useEffect(() => {
    get(`/api/events`).then(({ data: { data } }) => {
      setEvents(data)
    })
  }, [])

  return (
    <App>
      <NavBar events={events} />
      <Switch>
        <Route
          exact
          path="/"
          render={props => <Calendar {...props} events={events} />}
        />
        <Route
          exact
          path="/moderator/:id?"
          render={props => (
            <Moderator
              {...props}
              moderators={moderators}
              setModerators={setModerators}
            />
          )}
        />
        <Route
          exact
          path="/event/:id?"
          render={props => (
            <Event
              {...props}
              events={events}
              setEvents={setEvents}
              moderators={moderators}
            />
          )}
        />
        <Route
          exact
          path="/search"
          render={props => (
            <Search {...props} events={events} moderators={moderators} />
          )}
        />
        {/* <Route
          exact
          path="/settings"
          render={props => <Settings {...props} />}
        /> */}
      </Switch>
      <ToastContainer />
    </App>
  )
}
