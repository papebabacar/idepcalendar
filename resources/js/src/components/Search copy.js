import { Box, Input, Button, css } from '@smooth-ui/core-sc'
import Autosuggest from 'react-autosuggest'
import React, { useState } from 'react'

export default ({ events, moderators, setSelected }) => {
  const [searchTerm, setSearchTerm] = useState('')
  const [suggestions, setSuggestions] = useState([])

  const onSuggestionSelected = (_, { suggestion: { id } }) => setSelected(id)

  const onSuggestionsClearRequested = () => setSuggestions([])

  const onSuggestionsFetchRequested = ({ value }) => {
    setSuggestions(
      moderators.filter(({ email, firstname, lastname }) => {
        return (
          email
            .toLowerCase()
            .trim()
            .includes(value.toLowerCase().trim()) ||
          firstname
            .toLowerCase()
            .trim()
            .includes(value.toLowerCase().trim()) ||
          lastname
            .toLowerCase()
            .trim()
            .includes(value.toLowerCase().trim()) ||
          `${email}. ${lastname} ${firstname}`
            .toLowerCase()
            .trim()
            .includes(value.toLowerCase().trim())
        )
      })
    )
  }
  return (
    <Box
      css={css`
        margin: 0.40625em 1.625em;
        position: relative;
      `}
    >
      <Autosuggest
        suggestions={suggestions}
        shouldRenderSuggestions={shouldRenderSuggestions}
        onSuggestionsFetchRequested={onSuggestionsFetchRequested}
        onSuggestionsClearRequested={onSuggestionsClearRequested}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        onSuggestionSelected={onSuggestionSelected}
        inputProps={{
          value: searchTerm,
          onChange: (_, { newValue }) => {
            setSearchTerm(newValue)
            !newValue && setSelected(null)
          }
        }}
        renderInputComponent={inputProps => (
          <Input
            css={css`
              padding: 0.40625em 0.203125em;
              width: 100%;
            `}
            {...inputProps}
          />
        )}
      />
      <Button
        disabled={!searchTerm}
        css={css`
          color: #4040ec;
          position: absolute;
          right: 0;
          top: 0;
          font-size: 120%;
          padding: 0.5em;
          height: 100%;
          &:disabled {
            color: #aaa;
          }
        `}
        onClick={() => {
          setSearchTerm('')
          setSelected(null)
        }}
      >
        x
      </Button>
    </Box>
  )
}

const shouldRenderSuggestions = value => value.trim().length >= 2

const getSuggestionValue = ({ email, firstname, lastname }) =>
  `${email}. ${lastname} ${firstname}`

const renderSuggestion = ({ email, firstname, lastname }) => (
  <div>
    {email}. {lastname} {firstname}
  </div>
)
