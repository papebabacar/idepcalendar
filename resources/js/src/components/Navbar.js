import { Box, Button, css } from '@smooth-ui/core-sc'
import { Calendar as IconCalendar } from 'styled-icons/icomoon/Calendar'
import { PeopleOutline, EventNote } from 'styled-icons/material'
import { SearchAlt } from 'styled-icons/boxicons-regular'
import { FileDownload } from 'styled-icons/fa-solid'
import logo from '../../../img/logo.png'
import idep from '../../../img/idep.jpg'
import { Link } from 'react-router-dom'
import React from 'react'

export default ({ events }) => (
  <Box
    px="1.40625em"
    py="1.40625em"
    mb={1}
    as="nav"
    css={css`
      display: flex;
      align-items: center;
      background: url(${idep}), #006ab4;
      background-repeat: no-repeat;
      background-position: 55% 0;
    `}
  >
    <ul
      css={css`
        display: flex;
      `}
    >
      <li>
        <Button mr={1} variant="primary" as={Link} to="/moderator">
          <PeopleOutline size="24" />
          <span
            css={css`
              font-size: 80%;
              padding: 0.3125em;
            `}
          >
            Moderateurs
          </span>
        </Button>
      </li>
      <li>
        <Button mr={1} variant="primary" as={Link} to="/event">
          <EventNote size="24" />
          <span
            css={css`
              font-size: 80%;
              padding: 0.3125em;
            `}
          >
            Cours
          </span>
        </Button>
      </li>
      <li>
        <Button mr={1} variant="primary" as={Link} to="/search">
          <SearchAlt size="24" />
          <span
            css={css`
              font-size: 80%;
              padding: 0.3125em;
            `}
          >
            Rechercher
          </span>
        </Button>
      </li>
    </ul>

    <Box
      css={css`
        background: #2c2c2c0c;
        margin-left: auto;
      `}
    >
      {/* <img src={idep} /> */}
      <img src={logo} />
    </Box>

    <Box
      css={css`
        background: #2c2c2c0c;
        margin-left: auto;
      `}
    >
      <Button mr={1} variant="primary" as={Link} to="/">
        <IconCalendar size="20" />
        <span
          css={css`
            font-size: 80%;
            padding: 0.3125em;
          `}
        >
          Calendrier
        </span>
      </Button>
      <Button mr={1} variant="primary" onClick={() => download(events)}>
        <FileDownload size="18" />
        <span
          css={css`
            font-size: 80%;
            padding: 0.3125em;
          `}
        >
          Exporter
        </span>
      </Button>
    </Box>
  </Box>
)

const download = async events => {
  var cal = ics() // eslint-disable-line
  events.map(({ name, start, end, description, location }) => {
    cal.addEvent(name, description, location, start, end)
  })
  cal.download('Calendrier')
}
