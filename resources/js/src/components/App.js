import { Box, css } from '@smooth-ui/core-sc'

import React from 'react'

export default ({ children }) => {
  return (
    <Box
      css={css`
        background: linear-gradient(to right, #7474bf, #348ac7);
        height: 100vh;
        overflow-y: auto;
      `}
    >
      {children}
    </Box>
  )
}
