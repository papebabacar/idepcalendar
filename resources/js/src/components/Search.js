import { Box, Input, Button, css } from '@smooth-ui/core-sc'
import React, { useCallback, useRef } from 'react'
import debounce from 'lodash/debounce'

export default ({ searchTerm, setSearchTerm }) => {
  const search = term => setSearchTerm(term)
  const onChange = useCallback(debounce(search, 266.6666666666), [])
  const input = useRef()
  return (
    <Box
      css={css`
        margin: 0.40625em 1.625em;
        position: relative;
      `}
    >
      <Input
        ref={input}
        autoFocus
        onChange={e => onChange(e.target.value)}
        css={css`
          padding: 0.40625em 0.203125em;
          width: 100%;
        `}
      />
      <Button
        disabled={!searchTerm}
        css={css`
          color: #4040ec;
          position: absolute;
          right: 0;
          top: 0;
          font-size: 120%;
          padding: 0.5em;
          height: 100%;
          &:disabled {
            color: #aaa;
          }
        `}
        onClick={() => {
          setSearchTerm('')
          input.current.value = ''
          input.current.focus()
        }}
      >
        x
      </Button>
    </Box>
  )
}
