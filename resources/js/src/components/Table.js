import styled from 'styled-components'

export const Table = styled.table`
  display: block;
  border-radius: 5px;
  border: solid 1px #ddd;
  overflow-y: auto;
  height: 100%;
  width: 100%;
  table-layout: fixed;
`

export const Row = styled.tr`
  border-bottom: solid 1px #ddd;
  :last-child {
    border-bottom: 0;
  }
  :nth-of-type(odd) {
    background: #fcfcfc;
  }
`

export const HeaderRow = styled(Row)`
  border-bottom: solid 1px rgba(0, 0, 0, 0.2);
  background: #eee;
`

export const Cell = styled.td`
  border-right: solid 1px rgba(0, 0, 0, 0.1);
  padding: 0.40625em;

  :last-child {
    border-right: 0;
  }
`

export const Header = styled.th`
  text-align: left;
  font-weight: bold;
  border-right: solid 1px rgba(0, 0, 0, 0.1);
  padding: 0.40625em;
  :last-child {
    border-right: 0;
  }
`
