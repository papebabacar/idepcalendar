import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router } from 'react-router-dom'
import { Routes } from './routes'
import { ThemeProvider } from '@smooth-ui/core-sc'
import theme from './theme'
import 'react-toastify/dist/ReactToastify.css'

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Router>
      <Routes />
    </Router>
  </ThemeProvider>,
  document.getElementById('app')
)
