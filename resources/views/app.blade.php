<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>IDEP calendrier</title>
  <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
</head>

<body>
  <div id="app"></div>
  <script type="text/javascript">
    window.csrf_token = "{{ csrf_token() }}"
  </script>
  <script src="/js/lang-js"></script>
  <script src="{{mix('js/app.js')}}"></script>
  <script src="https://cdn.jsdelivr.net/gh/nwcell/ics.js@0.2.0/ics.deps.min.js"></script>
</body>

</html>